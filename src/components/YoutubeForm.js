import React from 'react';
import { Formik,Form, Field, ErrorMessage} from 'formik';
import * as Yup from 'yup'

const initialValues = {
    name: "",
    email: "",
    channel: ""
}
const onSubmit = values => {
    console.log("form values", values)
}

const validationSchema = Yup.object({
    name:Yup.string().required("Required!"),
    email:Yup.string().email('Invalid email format').required("Required!"),
    channel:Yup.string().required("Required!")
})

function YoutubeForm() {
   
    return (
        <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
        >
            <Form>
                <div className='form-control'>
                    <label htmlFor='name'>Name</label>
                    <Field type="text" id="name" name="name" 
                //   {...formik.getFieldProps('name')}
                     />
                     <ErrorMessage name="name"/>
                    {/* {formik.errors.name && formik.touched.name ? <div style={{color:"red"}}>{formik.errors.name}</div> : null} */}
                    <br />
                </div>
                <div className='form-control'>
                    <label htmlFor='email'>E-mail</label>
                    <Field type="email" id="email" name="email" 
                    // {...formik.getFieldProps('email')}
                    />
                   <ErrorMessage name="email"/>
                    {/* {formik.errors.email && formik.touched.email? <div style={{color:"red"}}>{formik.errors.email}</div> : null} */}
                    <br />
                </div>
                <div className='form-control'>
                    <label htmlFor='channel'>Channel</label>
                    <Field type="text" id="channel" name="channel"
                    // {...formik.getFieldProps('channel')}
                     />                     
                     <ErrorMessage name="channel"/>
                    {/* {formik.errors.channel && formik.touched.channel? <div style={{color:"red"}}>{formik.errors.channel}</div> : null} */}
                    <br />
                </div>
                <button type='submit'>Submit</button>
            </Form>
        </Formik>
    )
}

export default YoutubeForm